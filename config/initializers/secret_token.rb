# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
LunchTime::Application.config.secret_key_base = 'a99e897589d904c9db7bb48b41033b15a1051ec91bd667c6458d1f306885714a036f145941476c43ffe6dc2d341b1484dc4bd50c18afbe4398c6c6a495ef185b'
